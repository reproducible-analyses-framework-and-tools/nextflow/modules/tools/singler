#!/usr/bin/env nextflow

process singler_annotate {
// Runs SingleR annotation
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq2) - FASTQ 2
//   tuple
//     path(idx_files) - Index Files
//   parstr - Additional Parameters
//
// output:
//   tuple => emit: sams
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path('*.sam') - Output SAM File

// require:
//   FQS
//   IDX_FILES
//   params.singler$singler_count_parameters

  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/singler_annotate"
  tag "${dataset}/${pat_name}/${run}"
  label 'singler_container'
  label 'singler_annotate'
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(rds)
  path(sc_data)
  val(sc_dataset)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}.singler.rds"), emit: rdss

  script:
  """
  mkdir -p tmp

  echo "library(SingleR)" > singler_script.R
  echo "library(scRNAseq)" >> singler_script.R
  echo "library(scater)" >> singler_script.R
  echo "ExperimentHub::setExperimentHubOption('cache', paste0(getwd(), \\"/tmp\\"))" >> singler_script.R

  echo "sc_ref <- ${sc_dataset}()" >> singler_script.R

  # Removing unfineled cells or cells without a clear fine.
  echo "sc_ref <- sc_ref[,!is.na(sc_ref\\\$fine) & sc_ref\\\$fine!=\\"unclear\\"]" >> singler_script.R
  echo "sc_ref <- logNormCounts(sc_ref)" >> singler_script.R

  echo "so = readRDS(\\"${dataset}-${pat_name}-${run}.seurat.rds\\")" >> singler_script.R
  echo "ad = readRDS(\\"${dataset}-${pat_name}-${run}.seurat.assay_data.rds\\")" >> singler_script.R
  echo "rownames(ad) <- lapply(rownames(ad), sub, pattern = \\"\\\\\\\\.\\\\\\\\d+\\\$\\", replacement = \\"\\")" >> singler_script.R
  echo "pred.grun <- SingleR(test=ad, ref=sc_ref, labels=sc_ref\\\$fine, de.method=\\"wilcox\\", de.n=50)" >> singler_script.R

#  echo "so[['singler_annot']] = rep('NA', ncol(so))" >> singler_script.R
#  echo "so\\\$singler_main[rownames(pred.grun)] = pred.grun\\\$pruned.labels" >> singler_script.R

  echo "saveRDS(pred.grun, file = \\"${dataset}-${pat_name}-${run}.singler.rds\\")" >> singler_script.R

  Rscript singler_script.R
  """
}
